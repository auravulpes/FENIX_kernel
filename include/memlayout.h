/*
 * <memlayout.h> - Memory layout for Fenix
 * Largely based off memlayout.h from Xv6.
 */

#ifndef _MEMLAYOUT
#define _MEMLAYOUT

/*
 * For future reference on how memory works.
 * Extended memory starts at 0x100000 (1 MB).
 * It runs from there to some space below the
 * end of the actual physical memory. Here,
 * that space is defined at 0xE000000 (~224 MB).
 * 32-bit devices fill in from the end of
 * physical memory down, starting here at like
 * 256 MB and filling down to the top of
 * our available memory.
 */

/* TODO: Maybe we want more than 256 MB available? */
/*
  Counterpoint: This system isn't designed to use
  a lot of RAM, and it designed to be like a system
  of yore. 256 MB may be too much.
      -Kat
 */

#define EXTMEM 0x100000 /* Extended memory start */
#define PHYSTOP 0xE000000 /* Top of physical memory */
#define DEVSPACE 0xFE000000 /* 32-bit devices */

/* Important address space layout stuff  */
#define KERNBASE 0x80000000 /* First kernel virtual address */
#define KERNLINK (KERNBASE+EXTMEM) /* Kernel link address  */

/* Translations between physical and virtual memory */
#define V2P(a) (((uint) (a)) - KERNBASE)
#define P2V(a) ((void *)(((char *) (a)) + KERNBASE))

/* The above without casts (NC -> No Cast) */
#define V2P_NC(a) ((a) - KERNBASE)
#define P2V_NC(a) ((a) + KERNBASE)

#endif
