#ifndef _KERN_TTY_H
#define _KERN_TTY_H

#include <stddef.h>
#include <stdint.h>

#define BLACK 0
#define BLUE 1
#define GREEN 2
#define CYAN 3
#define RED 4
#define MAGENTA 5
#define BROWN 6
#define GREY 8
#define LGREY 7
#define LBLUE 9
#define LGREEN 10
#define LCYAN 11
#define LRED 12
#define LMAGENTA 13
#define LBROWN 14
#define WHITE 15

void term_init(void);
void term_scroll(void);
void term_putc(char c);
void term_setcolor(uint8_t fg, uint8_t bg);
void term_write(const char* str, size_t size);
void term_writestr(const char* str);

#endif
