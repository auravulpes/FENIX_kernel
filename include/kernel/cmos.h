#ifndef _KERNEL_CMOS
#define _KERNEL_CMOS

/* This is kinda important, and has to be changed each year. */
#define RELEASE_YEAR 2022

struct _rtc_val {
  unsigned char second;
  unsigned char minute;
  unsigned char hour;
  unsigned char day;
  unsigned char month;
  unsigned int year;
};

static struct _rtc_val cur_time;

enum {
  cmos_address = 0x70,
  cmos_data = 0x71
};

int _check_update_in_progress(void);
unsigned char _get_rtc_reg(unsigned char reg);
unsigned long int read_rtc();

#endif
