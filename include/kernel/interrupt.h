#ifndef _KERNEL_INTERRUPT
#define _KERNEL_INTERRUPT

#ifdef __GNUC__
struct IDT_entry {
  unsigned short int offset_lowerbits;
  unsigned short int selector;
  unsigned char zero;
  unsigned char type_attr;
  unsigned short int offset_higherbits;
} __attribute__((packed));
#else
struct IDT_entry {
  unsigned short int offset_lowerbits;
  unsigned short int selector;
  unsigned char zero;
  unsigned char type_attr;
  unsigned short int offset_higherbits;
};
#endif

#ifdef __GNUC__
__attribute((aligned(0x10)))
#endif
struct IDT_entry IDT[256];

void idt_init(void);

#endif
