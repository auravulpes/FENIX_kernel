#ifndef _KERNEL_INIT
#define _KERNEL_INIT

#include <stdio.h>

extern int tss[16][2];
extern unsigned short int * GDT;

int test_libc(void);
int init(void);

#endif
