#include <kernel/tty.h>
#include <kernel/init.h>
#include <sys/utsname.h>
#include <stdio.h>

void kern_main(void) {
  term_init();
  init();
  struct utsname u;
  uname(&u);
  printf("%s %s %s\n", u.sysname, u.release, u.machine);
}
