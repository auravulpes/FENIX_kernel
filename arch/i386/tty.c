#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <kernel/tty.h>

#include "vga.h"

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 24;
static uint16_t* const VGA_MEMORY = (uint16_t*) 0xC03FF000 ;

static size_t term_row;
static size_t term_col;
static uint8_t term_color;
static uint16_t* term_buf;

void term_init(void) {
  term_row = 0;
  term_col = 0;
  term_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
  term_buf = VGA_MEMORY;
  for(size_t y = 0; y < VGA_HEIGHT; y++) {   
    for(size_t x = 0; x < VGA_WIDTH; x++) {
      const size_t ind = y * VGA_WIDTH + x;
      term_buf[ind] = vga_entry(' ', term_color);
    }
  }
}

void term_scroll(void) {
  for(size_t y = 0; y < VGA_HEIGHT; y++) {
    for(size_t x = 0; x < VGA_WIDTH; x++) {
      const size_t ind = y * VGA_WIDTH + x;
      term_buf[ind] = term_buf[(y+1) * VGA_WIDTH + x];
    }
  }

  for(size_t x = 0; x < VGA_WIDTH; x++) {
    const size_t ind = VGA_HEIGHT * VGA_WIDTH + x;
    term_buf[ind] = vga_entry(' ', term_color);
  }
}

void term_setcolor(uint8_t fg, uint8_t bg) {
  term_color = vga_entry_color(fg, bg);
}

void term_putentryat(unsigned char c, uint8_t color, size_t x, size_t y) {
  const size_t ind = y * VGA_WIDTH + x;
  term_buf[ind] = vga_entry(c, color);
}

void term_putc(char c) {
  unsigned char uc = c;
  if(term_row > VGA_HEIGHT) {
    term_scroll();
    term_row--;
  }
  switch(uc) {
    case '\n': term_row++; term_col = 0; break;
    default: term_putentryat(uc, term_color, term_col, term_row); break;
  }
  
  if(uc != '\n' && ++term_col == VGA_WIDTH) {
    term_col = 0;
    if(++term_row == VGA_HEIGHT) {
      term_scroll();
    }
  }
}

void term_write(const char* str, size_t size) {
  for(size_t i = 0; i < size; i++) {
    term_putc(str[i]);
  }
}

void term_writestr(const char* str) {
  term_write(str, strlen(str));
}
