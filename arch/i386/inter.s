	.globl irq0
	.type irq0,%function
	.globl irq1
	.type irq1,%function
	.globl irq2
	.type irq2,%function
	.globl irq3
	.type irq3,%function
	.globl irq4
	.type irq4,%function
	.globl irq5
	.type irq5,%function
	.globl irq6
	.type irq6,%function
	.globl irq7
	.type irq7,%function
	.globl irq8
	.type irq8,%function
	.globl irq9
	.type irq9,%function
	.globl irq10
	.type irq10,%function
	.globl irq11
	.type irq11,%function
	.globl irq12
	.type irq12,%function
	.globl irq13
	.type irq13,%function
	.globl irq14
	.type irq14,%function
	.globl irq15
	.type irq15,%function

	.globl load_idt
	.type load_idt,%function

	.globl irq0_handler
	.globl irq1_handler
	.globl irq2_handler
	.globl irq3_handler
	.globl irq4_handler
	.globl irq5_handler
	.globl irq6_handler
	.globl irq7_handler
	.globl irq8_handler
	.globl irq9_handler
	.globl irq10_handler
	.globl irq11_handler
	.globl irq12_handler
	.globl irq13_handler
	.globl irq14_handler
	.globl irq15_handler

	.extern irq0_handler
	.extern irq1_handler
	.extern irq2_handler
	.extern irq3_handler
	.extern irq4_handler
	.extern irq5_handler
	.extern irq6_handler
	.extern irq7_handler
	.extern irq8_handler
	.extern irq9_handler
	.extern irq10_handler
	.extern irq11_handler
	.extern irq12_handler
	.extern irq13_handler
	.extern irq14_handler
	.extern irq15_handler

irq0:
	pushal
	call irq0_handler
	popal
	iret

irq1:	
	pushal
	cld
	call irq1_handler
	popal
	iret

irq2:	
	pushal
	call irq2_handler
	popal
	iret

irq3:
	pushal
	call irq3_handler
	popal
	iret

irq4:	
	pushal
	call irq4_handler
	popal
	iret

irq5:	
	pushal
	call irq5_handler
	popal
	iret

irq6:	
	pushal
	call irq6_handler
	popal
	iret

irq7:	
	pushal
	call irq7_handler
	popal
	iret

irq8:	
	pushal
	call irq8_handler
	popal
	iret

irq9:	
	pushal
	call irq9_handler
	popal
	iret

irq10:	
	pushal
	call irq10_handler
	popal
	iret

irq11:	
	pushal
	call irq11_handler
	popal
	iret

irq12:	
	pushal
	call irq12_handler
	popal
	iret

irq13:	
	pushal
	call irq13_handler
	popal
	iret

irq14:	
	pushal
	call irq14_handler
	popal
	iret

irq15:	
	pushal
	call irq15_handler
	popal
	iret

load_idt:
	mov 4(%esp), %edx
	lidt (%edx)
	sti
	ret
