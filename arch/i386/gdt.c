#include "gdt.h"

void encode_gdt_entry(unsigned short int * target, struct GDT source) {
  if((source.limit > 65536) && ((source.limit & 0xFFF) != 0xFFF)) {
    /* This needs to error out. */
    return;
  }
  if(source.limit > 65536) {
    /* Granularity adjustments */
    source.limit = source.limit >> 12;
    target[6] = 0xC0;
  }
  else {
    target[6] = 0x40;
  }

  /* Encode limit */
  target[0] = source.limit & 0xFF;
  target[1] = (source.limit >> 8) & 0xFF;
  target[6] |= (source.limit >> 16) & 0xF;

  /* Encode base */
  target[2] = source.base & 0xFF;
  target[3] = (source.base >> 8) & 0xFF;
  target[4] = (source.base >> 16) & 0xFF;
  target[7] = (source.base >> 24) & 0xFF;

  /* Encode type */
  target[5] = source.type;
}