#ifndef _ARCH_I386_PS2_KBD_H
#define _ARCH_I386_PS2_KBD_H

int scan_set;

unsigned char get_ps2_inbyte();

unsigned char* identify_device();

#endif