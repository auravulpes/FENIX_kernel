#ifndef _ARCH_I386_GDT_H
#define _ARCH_I386_GDT_H

struct GDT {
  unsigned int limit;
  unsigned long int base;
  unsigned short int type;
};

void encode_gdt_entry(unsigned short int *, struct GDT);
extern void set_gdt(unsigned short int[], int);
extern void reload_segments(void);

#endif
