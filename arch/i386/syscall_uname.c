#include <sys/utsname.h>
#include <string.h>

#define PTR ((struct utsname *) v)

int __syscall_uname(void * v) {
  strcpy(PTR->sysname, "FENIX");
  strcpy(PTR->release, "PRE-ALPHA");
  strcpy(PTR->version, "20220726_1700");
  strcpy(PTR->machine, "i386");

  return 0;
}