#include "ps2_kbd.h"
#include <sys/io.h>

unsigned char get_ps2_inbyte() {
  return inb(0x60);
}

unsigned char* identify_device() {
  unsigned char ret_val[2]; int j = 0;
  outb(0xF2, 0x64);
  for(unsigned char i; i != 0xFA; i = inb(0x60)) {
    ret_val[j++] = i;
  }
  return ret_val;
}